#!/usr/bin/env ruby
require 'rest-client'

REST_API_HEADERS = { 'Content-Type' => 'application/json' }.freeze

# TODO: 1. Replace with your APP ID and REST API Key

# Example: b2f7f966-d8cc-11e4-bed1-df8f05be55ba
ONESIGNAL_APP_ID = '4e27745d-7ff3-46b2-bf42-4d376ddc8a8d'.freeze

# Example: NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj
ONESIGNAL_REST_API_KEY = 'Basic MzMwOTMyODQtZjQ3YS00ZGYwLWEzNWEtNmVjMTdhMmJlMGZj'.freeze

# TODO: 2. Query your database and generate an array of hashes; structured like this:
email_player_id_list = [
  { email: 'shanlu@churinc.com', device_player_id: '2b9d0c37-267e-45fa-98d4-961686414365' },
#   { email: 'email_2@domain.com', device_player_id: '3b3e6417-b8fb-41b4-9b2b-552ee4818f12' }
]

email_player_id_list.each do |email_with_device|
  response = create_email(email_with_device)

  # TODO: (OPTIONAL):
  #    3. Save response[:id] as the player_id for the newly created email into your database
  if response[:success]
  end
end

def create_email(email_with_device)
  device = email_with_device

  email_auth_hash = OpenSSL::HMAC.hexdigest('sha256', ONESIGNAL_REST_API_KEY, device[:email])
  email_create_params = {
    device_type: 11,
    app_id: ONESIGNAL_APP_ID,
    identifier: device[:email],
    device_player_id: device[:device_player_id],
    email_auth_hash: email_auth_hash
  }

  email_player_id = nil

  begin
    # Create email record
    response = RestClient::Request.execute(
      method: :post,
      url: 'https://onesignal.com/api/v1/players',
      headers: REST_API_HEADERS,
      payload: email_create_params.to_json
    )

    email_player_id = JSON.parse(response.body)['id']
    device_update_params = {
      app_id: ONESIGNAL_APP_ID,
      email: device[:email],
      parent_player_id: email_player_id
    }

    # Link email to device
    RestClient::Request.execute(
      method: :put,
      url: "https://onesignal.com/api/v1/players/#{device[:device_player_id]}",
      headers: REST_API_HEADERS,
      payload: device_update_params.to_json
    )
  rescue => e
    puts "Error creating email record for: #{device[:email]}"
    puts e

    return { success: false, id: email_player_id }
  end

  { success: true, id: email_player_id }
end