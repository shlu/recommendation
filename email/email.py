import requests
import json

header = {"Content-Type": "application/json; charset=utf-8",
          "Authorization": "Basic NTRhN2M2OTMtY2QwZi00YTMzLTkzYjUtZTQyMmU1NGE2ODg2"}

payload = {"app_id": "4e27745d-7ff3-46b2-bf42-4d376ddc8a8d",
           "include_player_ids": ["2b9d0c37-267e-45fa-98d4-961686414365"],
           "email_subject": "Welcome to Cat Facts!",
           "email_body": "<html><head>Welcome to Cat Facts</head><body><h1>Welcome to Cat Facts<h1><h4>Learn more about everyone's favorite furry companions!</h4><hr/><p>Hi Nick,</p><p>Thanks for subscribing to Cat Facts! We can't wait to surprise you with funny details about your favorite animal.</p><h5>Today's Cat Fact (March 27)</h5><p>In tigers and tabbies, the middle of the tongue is covered in backward-pointing spines, used for breaking off and gripping meat.</p><a href='https://catfac.ts/welcome'>Show me more Cat Facts</a><hr/><p><small>(c) 2018 Cat Facts, inc</small></p><p><small><a href='[unsubscribe_url]'>Unsubscribe</a></small></p></body></html>"}

req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))

print(req.status_code, req.reason)