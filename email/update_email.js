const fetch = require('node-fetch');

/**
 * 1. Replace these values with your app ID and app's REST API key.
 */
const VARS = {
  ONESIGNAL_APP_ID: '4e27745d-7ff3-46b2-bf42-4d376ddc8a8d',
  ONESIGNAL_APP_REST_API_KEY: 'Basic MzMwOTMyODQtZjQ3YS00ZGYwLWEzNWEtNmVjMTdhMmJlMGZj'
};

async function run() {
  /**
   * 2. Query your database and generate an array of hashes; structured like this:
   */
  const emailToPushRecords = [
    /* Add pushRecordId if you have it */
    { emailAddress: 'shanlu@churinc.com', pushRecordId: '2b9d0c37-267e-45fa-98d4-961686414365' },
    // { emailAddress: 'email_2@domain.com' },
    // { emailAddress: 'email_3@domain.com', pushRecordId: 'baf37836-1549-44ae-8a17-e5e5de27f8cf' },
    // { emailAddress: 'email_4@domain.com' },
  ];
  
  if (VARS.ONESIGNAL_APP_ID.endsWith('>')) {
    console.error("Please update VARS.ONESIGNAL_APP_ID with your actual app ID.");
    return;
  }
  if (VARS.ONESIGNAL_APP_REST_API_KEY.endsWith('>')) {
    console.error("Please update VARS.ONESIGNAL_APP_REST_API_KEY with your actual app ID.");
    return;
  }

  for (const emailToPushRecord of emailToPushRecords) {
    const { success, emailRecordId } = await createEmailRecord(emailToPushRecord);
    if (typeof success !== 'undefined' && success) {
      console.log(`Email record for ${emailToPushRecord.emailAddress} now has record ID ${emailRecordId}.`);
    }
    else{
        console.log(`ERROR`);
    }

    /** (OPTIONAL) 3. Save emailRecordId as the player ID for your newly created email record into your database */
  }
}

function getEmailAuthHash(onesignalRestApiKey, emailAddress) {
  const crypto = require('crypto');
  const hmac = crypto.createHmac('sha256', onesignalRestApiKey);
  hmac.update(emailAddress);
  return hmac.digest('hex');
}

async function createEmailRecord(emailToPushRecord) {
  const { emailAddress, pushRecordId } = emailToPushRecord;
  const emailAuthHash = getEmailAuthHash(VARS.ONESIGNAL_APP_REST_API_KEY, emailAddress);

  let emailRecordId;

  try {
    /* Create an email record */
    {
      const response = await fetch(
        "https://onesignal.com/api/v1/players",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            app_id: VARS.ONESIGNAL_APP_ID,
            device_type: 11,
            identifier: emailAddress,
            device_player_id: pushRecordId,
            email_auth_hash: emailAuthHash,
          })
        }
      );
      emailRecordId = (await response.json())["id"];
    }

    if (pushRecordId) {
      /* Link the email record to the push record */
      {
        const response = await fetch(
          `https://onesignal.com/api/v1/players/${pushRecordId}`,
          {
            method: "PUT",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              app_id: VARS.ONESIGNAL_APP_ID,
              email: emailAddress,
              parent_player_id: emailRecordId,
            })
          }
        );
      }
    }
    
    if (emailRecordId) {
      return {
        success: true,
        emailRecordId: emailRecordId,
      };
    }
  } catch (e) {
    console.error(`Error while creating email record for ${emailToPushRecord.emailAddress}:`, e);
    return {
      success: false,
      emailRecordId: emailRecordId,
    };
  }
}

run();
