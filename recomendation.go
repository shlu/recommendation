package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var authorization string = "Basic MzMwOTMyODQtZjQ3YS00ZGYwLWEzNWEtNmVjMTdhMmJlMGZj"
var appID string = "\"4e27745d-7ff3-46b2-bf42-4d376ddc8a8d\""
var contentType = "application/json; charset=utf-8"

func sendPUSH(url string, request string, includedSegments string, contents string) string {
	data := []byte(`{"app_id" : ` + appID + `,
					"included_segments" : ` + includedSegments + `,
					"contents" : ` + contents + `}`)
	req, err := http.NewRequest(request, url, bytes.NewBuffer(data))

	// header of REQUEST
	req.Header.Set("Authorization", authorization)
	req.Header.Set("Content-Type", contentType)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	// get status and corresponding return values
	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
	jsonStr := string(body)
	returnValues := make(map[string]interface{})
	// get the notification id
	json.Unmarshal([]byte(jsonStr), &returnValues)
	id := fmt.Sprintf("%v", returnValues["id"])
	fmt.Println(id)
	return id

}

func sendEMAIL(url string, request string, includedSegments string, eSubject string, eBody string) string {
	data := []byte(`{"app_id" : ` + appID + `,
					"included_segments" : ` + includedSegments + `,
					"isEmail" : true,
					"email_subject" : ` + eSubject + `,
					"email_body" :` + eBody + `}`)
	req, err := http.NewRequest(request, url, bytes.NewBuffer(data))

	// header of REQUEST
	req.Header.Set("Authorization", authorization)
	req.Header.Set("Content-Type", contentType)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	// get status and corresponding return values
	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
	jsonStr := string(body)
	returnValues := make(map[string]interface{})
	// get the notification id
	json.Unmarshal([]byte(jsonStr), &returnValues)
	id := fmt.Sprintf("%v", returnValues["id"])
	fmt.Println(id)
	return id

}
