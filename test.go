package main

import (
	"fmt"
)

func main() {
	urlPush := "https://onesignal.com/api/v1/notifications"

	// body of POST
	fmt.Println("Going to send PUSH")
	// b := []byte(`{"app_id":"4e27745d-7ff3-46b2-bf42-4d376ddc8a8d",
	// 			"include_player_ids":["2b9d0c37-267e-45fa-98d4-961686414365"],` + `
	// 			// "included_segments" : ["Test Users"],
	// 			"contents":{"en": "EnglishMessage", "zh-Hans": "test"}}`)

	includeSeg := "[\"Test Users\"]"
	contents := "{\"en\": \"EnglishMessage\", \"zh-Hans\": \"test\"}"

	notificationID := sendPUSH(urlPush, "POST", includeSeg, contents)

	fmt.Println(notificationID)
}
