import requests
import json

header = {"Content-Type": "application/json; charset=utf-8",
          "Authorization": "Basic NTRhN2M2OTMtY2QwZi00YTMzLTkzYjUtZTQyMmU1NGE2ODg2"}

payload = { "app_id" : "4e27745d-7ff3-46b2-bf42-4d376ddc8a8d",
# "app_id": "5eb5a37e-b458-11e3-ac11-000c2940e62c",
           "include_player_ids": [
               "2b9d0c37-267e-45fa-98d4-961686414365"
               # "Test Users"
               # {"operator": "OR"}, {"field": "amount_spent", "relation": ">", "value": "0"}
           ],
           "contents": {"en": "English Message", "zh-Hans":"test"}}

req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))
print(req)
print(req.status_code, req.reason)