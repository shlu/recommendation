package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

var authorization string = "Basic MzMwOTMyODQtZjQ3YS00ZGYwLWEzNWEtNmVjMTdhMmJlMGZj"
var appID string = "\"4e27745d-7ff3-46b2-bf42-4d376ddc8a8d\""
var contentType = "application/json; charset=utf-8"

func sendPUSH(url string, request string, includedSegments string, contents string) string {
	data := []byte(`{"app_id" : ` + appID + `,
					"included_segments" : ` + includedSegments + `,
					"contents" : ` + contents + `}`)
	req, err := http.NewRequest(request, url, bytes.NewBuffer(data))

	// header of REQUEST
	req.Header.Set("Authorization", authorization)
	req.Header.Set("Content-Type", contentType)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	// get status and corresponding return values
	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
	jsonStr := string(body)
	returnValues := make(map[string]interface{})
	// get the notification id
	json.Unmarshal([]byte(jsonStr), &returnValues)
	id := fmt.Sprintf("%v", returnValues["id"])
	fmt.Println(id)
	return id

}

func main() {
	urlPush := "https://onesignal.com/api/v1/notifications"

	// body of POST
	fmt.Println("Going to send PUSH")
	// b := []byte(`{"app_id":"4e27745d-7ff3-46b2-bf42-4d376ddc8a8d",
	// 			"include_player_ids":["2b9d0c37-267e-45fa-98d4-961686414365"],` + `
	// 			// "included_segments" : ["Test Users"],
	// 			"contents":{"en": "EnglishMessage", "zh-Hans": "test"}}`)

	includeSeg := "[\"Test Users\"]"
	contents := "{\"en\": \"EnglishMessage\", \"zh-Hans\": \"test\"}"

	notificationID := sendPUSH(urlPush, "POST", includeSeg, contents)

	fmt.Println(notificationID)

	// get open information
	// url_open := "https://onesignal.com/api/v1/notifications/" + notification_id
	// open_body := []byte(`{"app_id":"4e27745d-7ff3-46b2-bf42-4d376ddc8a8d",
	// 			"opened": true}`)
	// req_open, _ := http.NewRequest("PUT", url_open, bytes.NewBuffer(open_body))

	// req_open.Header.Add("Content-Type", "application/json")

	// resp_open, err := http.DefaultClient.Do(req_open)
	// if err != nil {
	// 	panic(err)
	// }
	// defer resp_open.Body.Close()
	// fmt.Println("response Status:", resp_open.Status)
	// fmt.Println("response Headers:", resp_open.Header)
	// body_open, _ := ioutil.ReadAll(resp_open.Body)
	// fmt.Println("response Body:", string(body_open))

	// // get click information
	// url_noti_history := "https://onesignal.com/api/v1/notifications/" + notification_id + "/history"
	// hist_body := []byte(`{"app_id":"4e27745d-7ff3-46b2-bf42-4d376ddc8a8d",
	// 						"events": "clicked",
	// 						"email" : "shanlu@churinc.com"}`)
	// req_hist, _ := http.NewRequest("POST", url_noti_history, bytes.NewBuffer(hist_body))
	// req_hist.Header.Add("Content-Type", "application/json")
	// req_hist.Header.Add("Authorization", "Basic MzMwOTMyODQtZjQ3YS00ZGYwLWEzNWEtNmVjMTdhMmJlMGZj")
	// client_hist := &http.Client{}
	// resp_hist, err := client_hist.Do(req_hist)
	// if err != nil {
	// 	panic(err)
	// }
	// defer resp_hist.Body.Close()

	// // get status and corresponding return values
	// fmt.Println("response Status:", resp_hist.Status)
	// fmt.Println("response Headers:", resp_hist.Header)
	// body_hist, _ := ioutil.ReadAll(resp_hist.Body)
	// fmt.Println("response Body:", string(body_hist))
}
